const express = require('express')
const app = express()
const port = 3000

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// GET Show data only
app.get('/users', (req, res) => {
    var data =
        {
            users: [
                {
                    name:"khampasith",
                    phone: "58689898"     
                },
                {
                    name:"Mounoy",
                    phone: "77719192"     
                }
            ]
        }
  res.json(data)
})
// Insert Data 
app.post('/users', (req, res) => {
  res.json(req.body)
})

app.post('/v2/users', (req, res) => {
  res.json("Insert data")
})
// Update data
app.put('/users', (req, res) => {
  res.json("update data")
})

app.delete('/users', (req, res) => {
    res.json("delete data")
})
  

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})