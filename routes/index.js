var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/v1/laos/get-province', function(req, res, next) {
  var data =[
    {
        "pr_id": 1,
        "pr_name": "ແຂວງ ນະຄອນຫລວງວຽງຈັນ",
        "pr_name_en": "Vientiane capital"
    },
    {
        "pr_id": 2,
        "pr_name": "ແຂວງ ຜົ້ງສາລີ",
        "pr_name_en": "Phongsali"
    },
    {
        "pr_id": 3,
        "pr_name": "ແຂວງ ຫລວງນ້ຳທາ",
        "pr_name_en": "Louang Namtha"
    },
    {
        "pr_id": 4,
        "pr_name": "ແຂວງ ອຸດົມໄຊ",
        "pr_name_en": "Oudomxai"
    },
    {
        "pr_id": 5,
        "pr_name": "ແຂວງ ບໍ່ແກ້ວ",
        "pr_name_en": "Bokeo"
    },
    {
        "pr_id": 6,
        "pr_name": "ແຂວງ ຫຼວງພະບາງ",
        "pr_name_en": "Louang Phabang"
    },
    {
        "pr_id": 7,
        "pr_name": "ແຂວງ ຫົວພັນ",
        "pr_name_en": "Houaphan"
    },
    {
        "pr_id": 8,
        "pr_name": "ແຂວງ ໄຊຍະບູລີ",
        "pr_name_en": "Xaignabouli"
    },
    {
        "pr_id": 9,
        "pr_name": "ແຂວງ ຊຽງຂວາງ",
        "pr_name_en": "Xiangkhoang"
    },
    {
        "pr_id": 10,
        "pr_name": "ແຂວງ ວຽງຈັນ",
        "pr_name_en": "Vientiane"
    },
    {
        "pr_id": 11,
        "pr_name": "ແຂວງ ບໍລິຄຳໄຊ",
        "pr_name_en": "Boli khamxai"
    },
    {
        "pr_id": 12,
        "pr_name": "ແຂວງ ຄຳມ່ວນ",
        "pr_name_en": "Khammouan"
    },
    {
        "pr_id": 13,
        "pr_name": "ແຂວງ ສະຫວັນນະເຂດ",
        "pr_name_en": "Savannakhet"
    },
    {
        "pr_id": 14,
        "pr_name": "ແຂວງ ສາລະວັນ",
        "pr_name_en": "Salavan"
    },
    {
        "pr_id": 15,
        "pr_name": "ແຂວງ ເຊກອງ",
        "pr_name_en": "Xekong"
    },
    {
        "pr_id": 16,
        "pr_name": "ແຂວງ ຈຳປາສັກ",
        "pr_name_en": "Champasak"
    },
    {
        "pr_id": 17,
        "pr_name": "ແຂວງ ອັດຕະປື",
        "pr_name_en": "Attapu"
    },
    {
        "pr_id": 18,
        "pr_name": "ແຂວງ ໄຊສົມບູນ",
        "pr_name_en": "Sisomboun"
    }
];

  res.json(data);
});

router.post('/', function(req, res, next) {

  console.log(req.body)

 res.json({ title: 'Express Post', myreq: req.body});
});



module.exports = router;
